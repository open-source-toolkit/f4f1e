# cublasLt64-10.dll 资源文件下载

## 描述

本仓库提供了一个资源文件 `cublasLt64-10.dll`，该文件是解决 `Could not load dynamic library cublas64_10.dll` 报错的必要组件。通常情况下，只需从网上下载对应的 `.dll` 文件并将其放入 `C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA` 对应版本的 `bin` 目录下即可解决问题。然而，`cublas64_10.dll` 文件还需要 `cublasLt64_10.dll` 文件配合使用，否则仍会报错。

本仓库提供的资源文件 `cublasLt64-10.dll` 正是为了解决这一问题而准备的。您只需下载并解压该文件，然后将其放入上述的 `bin` 目录下即可。

## 使用方法

1. 点击仓库中的 `cublasLt64-10.dll` 文件进行下载。
2. 解压下载的文件。
3. 将解压后的 `cublasLt64-10.dll` 文件放入 `C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\<版本号>\bin` 目录下。
4. 重新启动您的应用程序，报错应该已经解决。

## 注意事项

- 请确保您下载的 `cublasLt64-10.dll` 文件与您的 CUDA 版本匹配。
- 如果您在其他目录下也遇到了类似的 `.dll` 文件缺失问题，可以尝试将该文件复制到相应的目录中。

## 贡献

如果您有任何问题或建议，欢迎提交 Issue 或 Pull Request。

## 许可证

本仓库中的资源文件遵循 [MIT 许可证](LICENSE)。